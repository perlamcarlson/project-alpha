from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("accounts_list")


def get_queryset(self):
    return self.model.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
  